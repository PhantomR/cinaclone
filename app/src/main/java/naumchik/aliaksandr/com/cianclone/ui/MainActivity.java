package naumchik.aliaksandr.com.cianclone.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import naumchik.aliaksandr.com.cianclone.App;
import naumchik.aliaksandr.com.cianclone.R;
import naumchik.aliaksandr.com.cianclone.helper.Router;
import naumchik.aliaksandr.com.cianclone.ui.fragment.MainFragment;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.FRAGMENT_TAG;

public class MainActivity extends AppCompatActivity{
    private Router mRouter = App.getInstance().getRouter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initTopFragment();
    }

    private void initTopFragment() {
        mRouter.replaceFragment(this, new MainFragment(), R.id.container, FRAGMENT_TAG, true);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
