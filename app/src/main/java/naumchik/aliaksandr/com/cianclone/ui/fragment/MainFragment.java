package naumchik.aliaksandr.com.cianclone.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import naumchik.aliaksandr.com.cianclone.R;
import naumchik.aliaksandr.com.cianclone.adapter.FuelStationsPagerAdapter;
import naumchik.aliaksandr.com.cianclone.db.FuelStation;
import naumchik.aliaksandr.com.cianclone.events.ChangePositionEvent;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.BOTTOM_SHEET_HEIGHT_CLOSED;
import static naumchik.aliaksandr.com.cianclone.utils.Constants.FS_TYPE_GAZ_PROM;
import static naumchik.aliaksandr.com.cianclone.utils.Constants.FS_TYPE_SHELL;

public class MainFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback {

    private static final int REQUEST_ACCESS_FINE_LOCATION = 0;

    private int mBottomSheetState = 3;
    private boolean mBottomSheetStatus;

    private LinearLayout mRootLayout;
    private BottomSheetBehavior mBottomSheetBehavior;

    private TabLayout mFuelStationsTabs;
    private ViewPager mFuelStationsViewPager;

    private EditText mSearchEditText;
    private ImageView mAppBarUserIcon;
    private ImageView mAppBarSearchIcon;
    private ImageView mAppBarSettingsIcon;
    private ImageView mAppBarPlusIcon;

    private ArrayList<FuelStation> fuelStations = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fillData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initListeners();
        initMaps();
        initBottomSheet();
        initViewPager();
    }

    @Override
    protected void initViews(View view) {
        CoordinatorLayout mCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator_layout);

        mSearchEditText = (EditText) view.findViewById(R.id.search_edit_text);

        mRootLayout = (LinearLayout) view.findViewById(R.id.root_layout_bottom_sheet);
        mFuelStationsViewPager = (ViewPager) view.findViewById(R.id.fuel_stations_view_pager);
        mFuelStationsTabs = (TabLayout) view.findViewById(R.id.fuel_stations_view_pager_tabs);

        mAppBarUserIcon = (ImageView) view.findViewById(R.id.app_bar_user_icon);
        mAppBarSearchIcon = (ImageView) view.findViewById(R.id.app_bar_search_icon);
        mAppBarSettingsIcon = (ImageView) view.findViewById(R.id.app_bar_settings_icon);
        mAppBarPlusIcon = (ImageView) view.findViewById(R.id.app_bar_plus_icon);

        hideSoftKeyboard(getActivity(), mCoordinatorLayout);
        setupUI(mCoordinatorLayout);
    }

    private void initListeners() {
        View[] views = new View[]{mRootLayout, mSearchEditText, mAppBarUserIcon, mAppBarSearchIcon,
                mAppBarSettingsIcon, mAppBarPlusIcon};
        for (View v : views) {
            v.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.root_layout_bottom_sheet:
                mBottomSheetStatus = !mBottomSheetStatus;
                checkBottomSheetStatus(mBottomSheetStatus);
                break;
            case R.id.search_edit_text:
                log("click on search_edit_text");
                closeBottomSheet();
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setMyLocationButtonEnabled(false);
        LatLng moscowCity = new LatLng(55.751244, 37.618423);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(moscowCity, 10));

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_ACCESS_FINE_LOCATION);
            }
            return;
        }

        map.setMyLocationEnabled(true);

        addCustomMarkers(map);
    }

    private void fillData() {
        Observable.fromCallable(() -> mFuelStationDatabase.getFuelStationDao().getAllFuelStations())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fuelStations -> {
                    log("getAllFuelStations: " + fuelStations.size());
                    if (fuelStations.size() == 0) {
                        initFuelStationList();
                    }
                });
    }

    private void initFuelStationList() {
        fuelStations.add(new FuelStation("Автозаправка Shell", "ул. Садовническая, 57", 1056,
                55.740749, 37.640577, "час назад", 0.3, FS_TYPE_SHELL));
        fuelStations.add(new FuelStation("Газпром", "ул. Карла-Маркса, 112", 825,
                55.650062, 37.323705, "2 часа назад", 1.3, FS_TYPE_GAZ_PROM));
        fuelStations.add(new FuelStation("Газпром", "ул. Дубнинская, 25", 825,
                55.880054, 37.557518, "7 часов назад", 3.7, FS_TYPE_GAZ_PROM));
        fuelStations.add(new FuelStation("Газпром", "ул. Люблинская, 141", 2157,
                55.647569, 37.747025, "12 часов назад", 1.5, FS_TYPE_GAZ_PROM));

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                mFuelStationDatabase.getFuelStationDao().insertAll(fuelStations);
                return null;
            }
        }.execute();
    }

    private void initMaps() {
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map))
                .getMapAsync(this);
    }

    private void addCustomMarkers(GoogleMap map) {
        log("add custom markers");
        if (map == null) {
            return;
        }

        Observable.fromCallable(() -> mFuelStationDatabase.getFuelStationDao().getAllFuelStations())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fuelStations -> {
                    for (int i = 0; i < fuelStations.size(); i++) {
                        FuelStation fuelStation = fuelStations.get(i);

                        Drawable drawable;
                        if (fuelStation.getType().equals(FS_TYPE_SHELL)) {
                            drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fuel_station_type_shell);
                        } else {
                            drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fuel_station_type_gazprom);
                        }
                        double lat = fuelStation.getLat();
                        double lng = fuelStation.getLng();

                        map.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lng))
                                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(drawable,
                                        fuelStation.getInfo(), fuelStation.getPrice()))));
                    }
                });
    }

    private Bitmap getMarkerBitmapFromView(Drawable drawable, String info, int price) {
        //get layout
        View customMarkerView = ((LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.view_custom_marker, null);

        //fill views
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.marker_fuel_station_type);
        TextView markerInfo = (TextView) customMarkerView.findViewById(R.id.marker_info);
        TextView markerPrice = (TextView) customMarkerView.findViewById(R.id.marker_price);

        markerImageView.setImageDrawable(drawable);
        markerInfo.setText(info);
        markerPrice.setText(String.valueOf(price));

        //draw
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();

        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(),
                customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawableBackground = customMarkerView.getBackground();
        if (drawableBackground != null) {
            drawableBackground.draw(canvas);
        }
        customMarkerView.draw(canvas);

        return returnedBitmap;
    }

    private void initBottomSheet() {
        mBottomSheetBehavior = BottomSheetBehavior.from(mRootLayout);
        closeBottomSheet();
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        closeBottomSheet();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        openBottomSheet();
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    private void checkBottomSheetStatus(boolean bottomSheetStatus) {
        if (bottomSheetStatus) {
            openBottomSheet();
        } else {
            closeBottomSheet();
        }
    }

    private void openBottomSheet() {
        mBottomSheetStatus = true;
        mBottomSheetState = BottomSheetBehavior.STATE_EXPANDED;
        mBottomSheetBehavior.setState(mBottomSheetState);
    }

    private void closeBottomSheet() {
        // it's not correct. Better take height of 1 item of recyclerView of ViewPager
        mBottomSheetBehavior.setPeekHeight(BOTTOM_SHEET_HEIGHT_CLOSED);

        mBottomSheetStatus = false;
        mBottomSheetState = BottomSheetBehavior.STATE_COLLAPSED;
        mBottomSheetBehavior.setState(mBottomSheetState);
    }

    private void initViewPager() {
        FuelStationsPagerAdapter mAdapter = new FuelStationsPagerAdapter(getChildFragmentManager());
        mFuelStationsViewPager.setAdapter(mAdapter);
        int selectedTabIndicatorColor = ContextCompat.getColor(getActivity(), R.color.viewPagerTabColor);
        mFuelStationsTabs.setSelectedTabIndicatorColor(selectedTabIndicatorColor);
        mFuelStationsTabs.setupWithViewPager(mFuelStationsViewPager);

        mFuelStationsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mEventBus.post(new ChangePositionEvent(position));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int num) {
            }
        });
    }

    @Subscribe
    public void onEvent(ChangePositionEvent changePositionEvent) {
        //need to investigate, looks like bug, but without it Bus don't see any @Subscribe public methods
    }
}
