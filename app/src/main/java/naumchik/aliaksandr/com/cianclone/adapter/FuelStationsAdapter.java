package naumchik.aliaksandr.com.cianclone.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import naumchik.aliaksandr.com.cianclone.R;
import naumchik.aliaksandr.com.cianclone.db.FuelStation;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.FS_TYPE_SHELL;

public class FuelStationsAdapter extends RecyclerView.Adapter<FuelStationsAdapter.FuelStationHolder> {

    private final Context mContext;
    private ArrayList<FuelStation> mFuelStations = new ArrayList<>();

    public FuelStationsAdapter(Context context, List<FuelStation> fuelStations) {
        this.mContext = context;
        this.mFuelStations = (ArrayList<FuelStation>) fuelStations;
    }

    public void setData(List<FuelStation> fuelStations) {
        this.mFuelStations = (ArrayList<FuelStation>) fuelStations;
        notifyDataSetChanged();
    }

    @Override
    public FuelStationHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View layoutView = LayoutInflater.from(
                viewGroup.getContext()).inflate(R.layout.item_fuel_station, viewGroup, false);
        return new FuelStationHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(FuelStationHolder holder, int position) {
        final FuelStation mFuelStation = this.mFuelStations.get(position);

        holder.mName.setText(mFuelStation.getName());
        holder.mInfo.setText(mFuelStation.getInfo());
        holder.mDistance.setText(String.valueOf(mFuelStation.getDistance()));
        holder.mPrice.setText(getChangedPriceView(mFuelStation));
        holder.mWhen.setText(mFuelStation.getTime());
        holder.mFuelStationType.setImageDrawable(getTypeDrawable(mFuelStation));
    }

    @Override
    public int getItemCount() {
        return mFuelStations.size();
    }

    private String getChangedPriceView(FuelStation fuelStation) {
        String price = String.valueOf(fuelStation.getPrice());
        int priceLength = price.length();
        if (priceLength > 3) {
            return price.substring(0, 1) + " " + price.substring(1, priceLength);
        } else {
            return price;
        }
    }

    private Drawable getTypeDrawable(FuelStation fuelStation) {
        if (fuelStation.getType().equals(FS_TYPE_SHELL)) {
            return ContextCompat.getDrawable(mContext, R.drawable.fuel_station_type_shell);
        } else {
            return ContextCompat.getDrawable(mContext, R.drawable.fuel_station_type_gazprom);
        }
    }

    class FuelStationHolder extends RecyclerView.ViewHolder {
        private TextView mName;
        private TextView mInfo;
        private TextView mPrice;
        private TextView mDistance;
        private TextView mWhen;
        private ImageView mFuelStationType;

        FuelStationHolder(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.fuel_st_name);
            mInfo = (TextView) view.findViewById(R.id.fuel_st_info);
            mPrice = (TextView) view.findViewById(R.id.fuel_st_price);
            mDistance = (TextView) view.findViewById(R.id.fuel_st_distance);
            mWhen = (TextView) view.findViewById(R.id.fuel_st_when);
            mFuelStationType = (ImageView) view.findViewById(R.id.fuel_st_type_img);
        }
    }
}
