package naumchik.aliaksandr.com.cianclone.utils;

public class Constants {

    // common
    public static final String FS_TYPE_SHELL = "FS_TYPE_SHELL";
    public static final String FS_TYPE_GAZ_PROM = "FS_TYPE_GAZ_PROM";
    public static final String FRAGMENT_TAG = "FRAGMENT_TAG";

    // view pager
    public static final String TAB_DISTANCE = "По расстоянию";
    public static final String TAB_PRICE = "По стоимости";
    public static final int PAGER_NUM_PAGES = 2;

    // bottom sheet
    public static final int BOTTOM_SHEET_HEIGHT_CLOSED = 225;

    // db
    public static final String FUEL_STATIONS_DB_NAME = "fuel_stations";
    public final static String QUERY_ALL_FUEL_STATIONS = "SELECT * FROM fuel_stations";
    public final static String QUERY_ALL_SORTED_BY_PRICE_FUEL_STATIONS = "SELECT * FROM " +
            "fuel_stations ORDER BY price ASC";
    public final static String QUERY_ALL_SORTED_BY_DISTANCE_FUEL_STATIONS = "SELECT * FROM " +
            "fuel_stations ORDER BY distance ASC";
}
