package naumchik.aliaksandr.com.cianclone.pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.Subscribe;

import naumchik.aliaksandr.com.cianclone.R;
import naumchik.aliaksandr.com.cianclone.adapter.FuelStationsAdapter;
import naumchik.aliaksandr.com.cianclone.events.ChangePositionEvent;
import naumchik.aliaksandr.com.cianclone.ui.fragment.BaseFragment;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FuelStationPage extends BaseFragment {

    private RecyclerView mFuelStationsRecyclerView;
    private FuelStationsAdapter mFuelStationsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.page_fuel_stations, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    @Override
    protected void initViews(View view) {
        mFuelStationsRecyclerView = (RecyclerView) view.findViewById(R.id.fuel_stations_recycler_view);
    }

    @Subscribe
    public void onEvent(ChangePositionEvent changePositionEvent) {
        int position = changePositionEvent.getPosition();
        if (position == 1) {
            Observable.fromCallable(() -> mFuelStationDatabase.getFuelStationDao().getSortedPriceFuelStations())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(fuelStations -> {
                        log("getSortedPriceFuelStations: " + fuelStations.size());
                        mFuelStationsAdapter.setData(fuelStations);
                    });
        } else if (position == 0)
            Observable.fromCallable(() -> mFuelStationDatabase.getFuelStationDao().getSortedDistanceFuelStations())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(fuelStations -> {
                        log("getSortedPriceFuelStations: " + fuelStations.size());
                        mFuelStationsAdapter.setData(fuelStations);
                    });
    }

    private void initRecyclerView() {
        Observable.fromCallable(() -> mFuelStationDatabase.getFuelStationDao().getSortedDistanceFuelStations())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fuelStations -> {
                    mFuelStationsAdapter = new FuelStationsAdapter(getActivity(), fuelStations);
                    mFuelStationsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mFuelStationsRecyclerView.setHasFixedSize(true);
                    mFuelStationsRecyclerView.setAdapter(mFuelStationsAdapter);
                });
    }
}
