package naumchik.aliaksandr.com.cianclone.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {FuelStation.class}, version = 1)
public abstract class FuelStationDatabase extends RoomDatabase {

    public abstract FuelStationDao getFuelStationDao();
}
