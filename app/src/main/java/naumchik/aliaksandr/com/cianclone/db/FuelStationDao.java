package naumchik.aliaksandr.com.cianclone.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import naumchik.aliaksandr.com.cianclone.utils.Constants;

@Dao
public interface FuelStationDao {

    @Query(Constants.QUERY_ALL_FUEL_STATIONS)
    List<FuelStation> getAllFuelStations();

    @Query(Constants.QUERY_ALL_SORTED_BY_PRICE_FUEL_STATIONS)
    List<FuelStation> getSortedPriceFuelStations();

    @Query(Constants.QUERY_ALL_SORTED_BY_DISTANCE_FUEL_STATIONS)
    List<FuelStation> getSortedDistanceFuelStations();

    @Insert
    void insertAll(List<FuelStation> fuelStations);

    @Insert
    void insert(FuelStation fuelStation);

    @Delete
    void delete(FuelStation fuelStation);
}
