package naumchik.aliaksandr.com.cianclone.helper;

import org.greenrobot.eventbus.EventBus;

public class SingletonBus {

    private volatile static SingletonBus instance;
    private EventBus bus;

    private SingletonBus() {
        this.bus = new EventBus();
    }

    public static SingletonBus getInstance() {
        if (instance == null) {
            synchronized (SingletonBus.class) {
                if (instance == null) {
                    instance = new SingletonBus();
                }
            }
        }
        return instance;
    }

    public <T> void post(final T event) {
        bus.post(event);
    }

    public <T> void register(T subscriber) {
        bus.register(subscriber);
    }

    public <T> void unregister(T subscriber) {
        bus.unregister(subscriber);
    }
}
