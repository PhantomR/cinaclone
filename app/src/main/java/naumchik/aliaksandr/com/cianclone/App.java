package naumchik.aliaksandr.com.cianclone;

import android.arch.persistence.room.Room;
import android.support.multidex.MultiDexApplication;

import lombok.Getter;
import naumchik.aliaksandr.com.cianclone.db.FuelStationDatabase;
import naumchik.aliaksandr.com.cianclone.helper.Router;
import naumchik.aliaksandr.com.cianclone.helper.SingletonBus;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.FUEL_STATIONS_DB_NAME;

public class App extends MultiDexApplication {

    private static App mApp;
    @Getter
    private Router router;
    @Getter
    private SingletonBus singletonBus;
    @Getter
    private FuelStationDatabase fuelStationDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        initializeSingletons();
    }

    private void initializeSingletons() {
        router = Router.getInstance();
        singletonBus = SingletonBus.getInstance();

        fuelStationDatabase = Room.databaseBuilder(
                getApplicationContext(),
                FuelStationDatabase.class,
                FUEL_STATIONS_DB_NAME).build();
    }

    public static App getInstance() {
        return mApp;
    }
}
