package naumchik.aliaksandr.com.cianclone.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.FUEL_STATIONS_DB_NAME;

@Entity(tableName = FUEL_STATIONS_DB_NAME)
public class FuelStation {

    @PrimaryKey (autoGenerate = true)
    private long id;
    private String name;
    private String info;
    private int price;
    private double lat;
    private double lng;
    private String time;
    private double distance;
    private String type;

    public FuelStation(String name, String info, int price, double lat, double lng, String time, double distance, String type) {
        this.name = name;
        this.info = info;
        this.price = price;
        this.lat = lat;
        this.lng = lng;
        this.time = time;
        this.distance = distance;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public String getInfo() {
        return info;
    }

    public int getPrice() {
        return price;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getTime() {
        return time;
    }

    public double getDistance() {
        return distance;
    }

    public String getType() {
        return type;
    }
}
