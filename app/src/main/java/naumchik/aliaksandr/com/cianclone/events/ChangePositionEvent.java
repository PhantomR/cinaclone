package naumchik.aliaksandr.com.cianclone.events;

import lombok.Getter;

public class ChangePositionEvent {

    @Getter
    private final int position;

    public ChangePositionEvent(int position) {
        this.position = position;
    }
}
