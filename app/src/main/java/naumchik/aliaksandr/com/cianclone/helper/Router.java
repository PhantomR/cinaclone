package naumchik.aliaksandr.com.cianclone.helper;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import naumchik.aliaksandr.com.cianclone.ui.fragment.BaseFragment;

public class Router {

    private volatile static Router instance;

    public static Router getInstance() {
        if (instance == null) {
            synchronized (Router.class) {
                if (instance == null) {
                    instance = new Router();
                }
            }
        }
        return instance;
    }

    public void replaceFragment(FragmentActivity activity,
                                BaseFragment fragment,
                                int containerId,
                                String tag,
                                boolean addToStack) {

        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(containerId, fragment, tag);
        processWithStack(activity, transaction, addToStack);
        transaction.commit();
    }

    private void processWithStack(Activity activity,
                                  FragmentTransaction transaction,
                                  boolean addToStack) {

        if (addToStack)
            transaction.addToBackStack(null);

        else if (activity.getFragmentManager().getBackStackEntryCount() > 0) {
            activity.getFragmentManager().popBackStack();
            transaction.addToBackStack(null);
        }
    }
}