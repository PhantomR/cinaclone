package naumchik.aliaksandr.com.cianclone.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import naumchik.aliaksandr.com.cianclone.pager.FuelStationPage;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.PAGER_NUM_PAGES;
import static naumchik.aliaksandr.com.cianclone.utils.Constants.TAB_DISTANCE;
import static naumchik.aliaksandr.com.cianclone.utils.Constants.TAB_PRICE;

public class FuelStationsPagerAdapter extends FragmentPagerAdapter {

    public FuelStationsPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence title = null;
        switch (position) {
            case 0:
                title = TAB_DISTANCE;
                break;
            case 1:
                title = TAB_PRICE;
                break;
        }
        return title;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new FuelStationPage();
                break;
            case 1:
                fragment = new FuelStationPage();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGER_NUM_PAGES;
    }
}