package naumchik.aliaksandr.com.cianclone;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import naumchik.aliaksandr.com.cianclone.db.FuelStation;
import naumchik.aliaksandr.com.cianclone.db.FuelStationDao;
import naumchik.aliaksandr.com.cianclone.db.FuelStationDatabase;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


@RunWith(AndroidJUnit4.class)
public class FuelStationDatabaseTest extends BaseTests {

    private FuelStationDao mFuelStationDao;
    private FuelStationDatabase mFuelStationDatabase;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mFuelStationDatabase = Room.inMemoryDatabaseBuilder(context, FuelStationDatabase.class)
                .build();
        mFuelStationDao = mFuelStationDatabase.getFuelStationDao();
    }

    @After
    public void closeDb() throws IOException {
        mFuelStationDatabase.close();
    }

    @Test
    public void writeFuelStationAndReadInList() throws Exception {
        FuelStation fuelStation = mTestUtil.getTestFuelStation();
        mFuelStationDao.insert(fuelStation);

        FuelStation readed = mFuelStationDao.getAllFuelStations().get(0);

        assertNotNull(readed);

        assertEquals(fuelStation.getName(), readed.getName());
        assertEquals(fuelStation.getDistance(), readed.getDistance());
        assertEquals(fuelStation.getInfo(), readed.getInfo());
        assertEquals(fuelStation.getLat(), readed.getLat());
        assertEquals(fuelStation.getLng(), readed.getLng());
        assertEquals(fuelStation.getPrice(), readed.getPrice());
        assertEquals(fuelStation.getTime(), readed.getTime());
        assertEquals(fuelStation.getType(), readed.getType());
    }
}