package naumchik.aliaksandr.com.cianclone;

import android.support.test.rule.ActivityTestRule;
import android.support.v4.app.Fragment;

import junit.framework.Assert;

import naumchik.aliaksandr.com.cianclone.ui.MainActivity;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.FRAGMENT_TAG;

class FragmentTestRule<F extends Fragment> extends ActivityTestRule<MainActivity> {

    private final Class<F> mFragmentClass;
    private F mFragment;

    FragmentTestRule(final Class<F> fragmentClass) {
        super(MainActivity.class, true, false);
        mFragmentClass = fragmentClass;
    }

    @Override
    protected void afterActivityLaunched() {
        super.afterActivityLaunched();

        getActivity().runOnUiThread(() -> {
            try {
                //Instantiate and insert the fragment into the container layout
                mFragment = mFragmentClass.newInstance();

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, mFragment, FRAGMENT_TAG)
                        .commitAllowingStateLoss();
            } catch (InstantiationException | IllegalAccessException e) {
                Assert.fail(String.format("%s: Could not insert %s into TestActivity: %s",
                        getClass().getSimpleName(),
                        mFragmentClass.getSimpleName(),
                        e.getMessage()));
            }
        });
    }

    public F getFragment() {
        return mFragment;
    }
}