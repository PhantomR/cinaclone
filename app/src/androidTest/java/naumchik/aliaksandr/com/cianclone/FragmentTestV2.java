package naumchik.aliaksandr.com.cianclone;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import naumchik.aliaksandr.com.cianclone.ui.MainActivity;
import naumchik.aliaksandr.com.cianclone.ui.fragment.MainFragment;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static naumchik.aliaksandr.com.cianclone.utils.Constants.FRAGMENT_TAG;
import static org.hamcrest.core.AllOf.allOf;

public class FragmentTestV2 extends BaseTests {

    private  String mStringToBeTyped;
    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        TestUtil.wakeUpDevice();
        mStringToBeTyped = TestUtil.getMockString();
        initMainFragment(new MainFragment());
//        initRecyclerView();
    }

    @Test
    public void testFragmentUI() throws Throwable {
        onView(withId(R.id.search_edit_text))
                .perform(typeText(mStringToBeTyped), closeSoftKeyboard())
                .check(matches(withText(mStringToBeTyped)));

        onView(withText(mStringToBeTyped))
                .check(matches(isDisplayed()));

        onView(allOf(withId(R.id.fuel_stations_recycler_view), isDisplayed()))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }

//    private void initRecyclerView() {
//        RecyclerView recyclerView = Mockito.mock(RecyclerView.class);
//
//        List<FuelStation> fuelStations = new ArrayList<>();
//        FuelStation fuelStation = Mockito.mock(FuelStation.class);
//        fuelStations.add(fuelStation);
//
//        FuelStationsAdapter adapter = Mockito.mock(FuelStationsAdapter.class);
//
//        adapter.setData(fuelStations);
//        recyclerView.setAdapter(adapter);
//    }

    private void initMainFragment(MainFragment mainFragment) {
        MainActivity mActivity = activityRule.getActivity();

        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(mainFragment, FRAGMENT_TAG)
                .commitAllowingStateLoss();
    }
}