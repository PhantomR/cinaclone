package naumchik.aliaksandr.com.cianclone;

import org.junit.Before;

/**
 * Description.
 *
 * @author Aliaksandr Naumchik
 */

abstract class BaseTests {
    TestUtil mTestUtil;

    @Before
    public void initDataMock() {
        mTestUtil = new TestUtil();
    }
}
