package naumchik.aliaksandr.com.cianclone;

import android.graphics.Point;
import android.os.RemoteException;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.UiDevice;

import java.util.ArrayList;
import java.util.List;

import naumchik.aliaksandr.com.cianclone.db.FuelStation;

import static naumchik.aliaksandr.com.cianclone.utils.Constants.FS_TYPE_GAZ_PROM;
import static naumchik.aliaksandr.com.cianclone.utils.Constants.FS_TYPE_SHELL;

class TestUtil {

    TestUtil() {
    }

    static String getMockString(){
        return "Test";
    }

    FuelStation getTestFuelStation() {
        return new FuelStation("Автозаправка Shell", "ул. Садовническая, 57", 1056,
                55.740749, 37.640577, "час назад", 0.3, FS_TYPE_SHELL);
    }

    List<FuelStation> getTestFuelStations() {
        List<FuelStation> fuelStations = new ArrayList<>();
        fuelStations.add(new FuelStation("Автозаправка Shell", "ул. Садовническая, 57", 1056,
                55.740749, 37.640577, "час назад", 0.3, FS_TYPE_SHELL));
        fuelStations.add(new FuelStation("Газпром", "ул. Карла-Маркса, 112", 825,
                55.650062, 37.323705, "2 часа назад", 1.3, FS_TYPE_GAZ_PROM));
        fuelStations.add(new FuelStation("Газпром", "ул. Дубнинская, 25", 825,
                55.880054, 37.557518, "7 часов назад", 3.7, FS_TYPE_GAZ_PROM));
        fuelStations.add(new FuelStation("Газпром", "ул. Люблинская, 141", 2157,
                55.647569, 37.747025, "12 часов назад", 1.5, FS_TYPE_GAZ_PROM));

        return fuelStations;
    }

    static void wakeUpDevice() {
        UiDevice uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        Point[] coordinates = new Point[4];
        coordinates[0] = new Point(248, 1520);
        coordinates[1] = new Point(248, 929);
        coordinates[2] = new Point(796, 1520);
        coordinates[3] = new Point(796, 929);
        try {
            if (!uiDevice.isScreenOn()) {
                uiDevice.wakeUp();
                uiDevice.swipe(coordinates, 10);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
