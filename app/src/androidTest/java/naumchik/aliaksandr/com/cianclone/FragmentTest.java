package naumchik.aliaksandr.com.cianclone;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import naumchik.aliaksandr.com.cianclone.ui.fragment.MainFragment;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class FragmentTest extends BaseTests{

    @Rule
    public FragmentTestRule<MainFragment> mFragmentTestRule = new FragmentTestRule<>(MainFragment.class);

    @Before
    public void setup() {
        TestUtil.wakeUpDevice();
        mFragmentTestRule.launchActivity(null);
    }

    @Test
    public void testFragment() {
        onView(withId(R.id.search_edit_text)).check(matches(isDisplayed()));
        onView(withId(R.id.search_edit_text)).perform(typeText(TestUtil.getMockString()), closeSoftKeyboard());
    }
}