package naumchik.aliaksandr.com.cianclone;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class ApplicationTest {

    @Test
    public void useAppContext() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();

        assertEquals("naumchik.aliaksandr.com.cianclone", context.getPackageName());
    }
}